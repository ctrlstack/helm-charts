# Helm charts

## Pre-requisites

To gain full visibility into your Kubernetes cluster, install ``kube-state-metrics``.
kube-state-metrics is a simple service that listens to the Kubernetes API server and generates metrics about the state of the objects, which are used by the ``ctrlstack-agent``.

```console
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install kube-state-metrics prometheus-community/kube-state-metrics -n default
```

## Install repository

Add a chart helm repository with follow commands:

```console
helm repo add ctrlstack https://ctrlstack.gitlab.io/helm-charts/
helm repo update
```

## Installing the chart

Export default values of ``ctrlstack-agent`` chart to file ``values.yaml``:

```console
helm show values ctrlstack/ctrlstack-agent > values.yaml
```

Edit ``values.yaml`` file changing the values accordingly. The only required settings are the credentials used to authorize the agent.

Install the chart:

```console
helm install ctrlstack-agent ctrlstack/ctrlstack-agent -f values.yaml
```

