#!/bin/bash -x

usage() {
    echo "Package and publish a new version of a helm chart."
    echo
    echo "Syntax: package.sh <chart-dir> <chart-alias>"
    echo
}

error_exit()
{
  echo "$1" 1>&2
  exit 1
}

if [[ $# -ne 2 ]]; then
    usage
    exit 1
fi

# Source: https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $SCRIPT_DIR/..

CHART=$1
CHART_DIR=charts/$CHART
CHART_ALIAS=$2

if [[ ! -d $CHART_DIR ]]; then
    error_exit "no matching chart found in services dir."
fi

if [[ ! -f $CHART_DIR/Chart.yaml ]]; then
    error_exit "no Chart.yaml found in ${CHART_DIR}....aborting."
fi

helm package ${CHART_DIR}
helm cm-push ${CHART_ALIAS}*.tgz ${CHART}
